<?php

namespace App\Http\Controllers\SV;

use App\BL\SV\VirtualService\VirtualServiceBL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Generic\ResponseController;
use App\Http\Requests\SV\VirtualServiceRequest;

/** 
 * Controlador para las gestiones de los servicios virtuales
 * 
 * 
 * @package App\Http\Controllers\SV
 * @author Luis Manuel Hernandez Jimenez (luis.hernandez@grupokonecta.com)
 * @date 01/01/2016
 * */
class VirtualServiceController extends Controller
{

    /**
     * Metodo para registrar un servicio virtual
     * Method to login a virtual service
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param VirtualServiceRequest $request
     * @return json $response
     */
    public function addService(VirtualServiceRequest $request) {
        $objRequest = $request->all();
        $response = VirtualServiceBL::add($objRequest);
        return ResponseController::objectResponse($response, 200);
    }

    public function update(VirtualServiceRequest $request) {
        $data = $request->all();
        $response = VirtualServiceBL::update($data);
        return ResponseController::objectResponse($response, 200);
    }

    public function reactive(VirtualServiceRequest $request) {
        $data = $request->all();
        $response = VirtualServiceBL::reactive($data);
        return ResponseController::objectResponse($response, 200);
    }

    public function delete() {
        $objRequest = request()->get('id');
        $response = VirtualServiceBL::delete($objRequest);
        return ResponseController::objectResponse($response, 200);
    }

    public function getAll() {
        $response = VirtualServiceBL::getAll();
        return ResponseController::objectResponse($response, 200);
    }

    public function getAllusersSV() {
        $response = VirtualServiceBL::getAllusersSV();
        return ResponseController::objectResponse($response, 200);
    }

    public function getServiceById(Request $request) {
        $response = VirtualServiceBL::getServiceById($request->id);
        return ResponseController::objectResponse($response, 200);
    }

}
