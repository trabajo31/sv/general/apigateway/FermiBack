<?php

namespace App\Http\Requests\SV;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class VirtualServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'firstName' => 'max:20|regex:/^[\pL\s]*$/u',
            // 'subject' => 'max:50',
            // 'userData.GCTI_LanguageCode' => 'max:20',
            // 'userData.IpOrigen' => 'max:20',
            // 'userData.segmento' => 'max:20',
            // 'userData.tipoId' => 'max:20',
            // 'userData.Identify' => 'max:20',
            // 'userData.EmailAddress' => 'max:20',
            // 'userData.PhoneNumber' => 'max:20',
            // 'userData.sede' => 'max:20',
            // 'userData.City' => 'max:20',
            // 'userData.estado' => 'max:20',
            // 'userData.motivopsa' => 'max:20',
            // 'userData.motivoc' => 'max:20',
            // 'userData.IpOrigen' => 'max:20',
            // 'userData.terminos' => 'max:20',
            // 'userData.Send_Chat_Transcript' => 'max:20',
            // 'userData.FiltroCorreo' => 'max:20',
            // 'userData.sitio' => 'max:20',
            // 'userData._genesys_source' => 'max:20',
            // 'userData._genesys_referrer' => 'max:20',
            // 'userData._genesys_url' => 'max:255',
            // 'userData._genesys_pageTitle' => 'max:20',
            // 'userData._genesys_browser' => 'max:20',
            // 'userData._genesys_OS' => 'max:20',
            // 'userData._genesys_widgets' => 'max:20',
        ];
    }

    public function messages()
    {
        return [
            // 'user.user_name.required' => 'El Usuario de red es obligatoria.',
            // 'user.user_name.max'      => 'El campo Usuario de red  es de maximo 45 caracteres',
            // 'user.user_name.regex'      => 'El campo Usuario de red debe ser texto',
            // 'user.name.required' => 'El Nombre es obligatoria.',
            // 'user.name.max'      => 'El campo Nombre de red es de maximo 100 caracteres',
            // 'user.name.regex'      => 'El campo Nombre de red debe ser solo letras',
            // 'user.area_id.required' => 'La Gerencia es obligatoria.',
            // 'user.email.email'      => 'El Email debe ser valido.',
            // 'user.email.max'      => 'El campo Email es de maximo 100 caracteres',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        
        throw new HttpResponseException(response()->json([
            'status' => 422,
            'errors' => $validator->errors()->all()
        ], 200));
    }
}
