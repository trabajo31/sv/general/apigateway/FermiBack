<?php

namespace App\BL\Client;

use App\Http\Controllers\Generic\ResponseController;
use Illuminate\Support\Facades\Log;
use App\AO\CLiente\ClientAO;
use App\Helpers\CurrentUser\CurrentUser;

class ClientBL {

    private static $response = [];
    private static $excepcion = ['msm' => 'Error al consultar en la Base de Datos', 'status' => 500];
    
    /**
     * Metodo para crear un nuevo cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param array $data Datos del cliente a instertad en BD
     * @return Response App\Http\Controllers\Generic\ResponseController
     */
    public static function add($data) {
        try {
            $client = $data['name'];
            $data_user = ClientAO::getClientByName($client);
            if(!empty($data_user) > 0){
                self::$response = ['data' => null,'msn' => 'El cliente ya existe en el sistema', 'status' => 202];
            }else{
                $currentUserId = CurrentUser::getCurrentUser()->id;
                $data['id_user_edit'] = $currentUserId;
                $data['id_user_create'] = $currentUserId;
                $data['delected'] = 0;
                $client = ClientAO::add($data);
                if($client) {
                    self::$response = ['data' => $client, 'msn' => 'Cliente creado!', 'status' => 200];
                    Log::info("Successful consultation -> function add()");
                } else {
                    self::$response = ['msn' => 'El cliente no pudo ser creado', 'status' => 500];
                    Log::info("Successful consultation -> function add()");
                }
            }
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function add()");
        }
        return self::$response;
    }

    /**
     * Metodo para actualizar la información de un nuevo cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param array $data Datos del cliente a actualizar
     * @return Response App\Http\Controllers\Generic\ResponseController
     */
    public static function update($data) {
        try {
            $data_user = ClientAO::getClientById($data['id']);
            if($data_user){
                $data['id_user_edit'] = CurrentUser::getCurrentUser()->id;
                $objData = ClientAO::update($data_user, $data);
                self::$response = ['data' => $objData, 'msn' => 'Cliente actualizado!', 'status' => 200];
            }else{
                $arrResponse = array('user' => $data_user, 'msn' => 'El usuario no existe');
                self::$response = ['data' => $arrResponse, 'status' => 400];
            }
            Log::info("Record updated -> function update()");
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function update()");
        }
        return self::$response;
    }

    /**
     * Metodo para eliminar un cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param id $id Datos del cliente a instertad en BD
     * @return Response App\Http\Controllers\Generic\ResponseController
     */
    public static function delete($id) {
        try {

            $objData = ClientAO::getClientById($id);
            if($objData) {
                $objData = ClientAO::delete($objData);
                self::$response = ['data' => $objData, 'msn' => 'Cliente eliminado!','status' => 200];
                Log::info("Record deleted -> function delete()");
            } else {
                self::$response = ['msn' => 'El cliente no existe en bd','status' => 400];
                Log::info("Record deleted -> function delete()");
            }
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function delete()");
        }
        return self::$response;
    }

    /**
     * Metodo para reactivar un cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param id $id Datos del cliente a instertad en BD
     * @return Response App\Http\Controllers\Generic\ResponseController
     */
    public static function reactive($id) {
        try {

            $objData = ClientAO::getClientDeleteById($id);
            if($objData) {
                $objData = ClientAO::reactive($objData);
                self::$response = ['data' => $objData, 'msn' => 'Cliente reactivado!','status' => 200];
                Log::info("Record reactive -> function reactive()");
            } else {
                self::$response = ['msn' => 'El cliente se encuentra activo o no existe en bd','status' => 400];
                Log::info("Record reactive -> function reactive()");
            }
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function reactive()");
        }
        return self::$response;
    }

    /**
     * Metodo para obtener todos los cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param id $id Datos del cliente a instertad en BD
     * @return Response App\Http\Controllers\Generic\ResponseController
     */
    public static function getAll() {
        try {
            $objData = ClientAO::getAll();
            self::$response = ['data' => $objData, 'msn' => 'Exito','status' => 200];
            Log::info("Record deleted -> function getAll() in app\BL\Client\ClientBL.php");
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function getAll() in app\BL\Client\ClientBL.php");
        }
        return self::$response;
    }

}
