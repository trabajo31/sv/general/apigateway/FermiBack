<?php

namespace App\Helpers\TokenUser;

use App\BL\User\TokenUserBL;

class JwtAuthHelperUser
{
    public function saveToken($infoToken){
        TokenUserBL::setToken($infoToken);
    }
    
    public function checkToken($infoToken){
        return TokenUserBL::validateConversation($infoToken);
    }

    
}