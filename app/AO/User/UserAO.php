<?php

namespace App\AO\User;

use App\Models\User;
use App\Models\CategoryUser\CategoryUser;
use Illuminate\Support\Facades\DB;

class UserAO
{
    /**
     * Consulta para obtener un usuario por nombre de usuario de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param string $userNetwork usuario de red del usuario a buscar
     * @return Modelo App\Models\User
     */
    public static function getUserByUserNetwork($userNetwork) {
        return User::where('user_network', $userNetwork)->first();
    }

    /**
     * Consulta para obtener un usuario por id de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param id $id ID del usuario a buscar
     * @return Modelo App\Models\User
     */
    public static function getUserById($id) {
        return User::where('id', $id)->first();
    }

    /**
     * Consulta para almacenar un usuario en la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param array $user datos a almacenar
     * @return boolean
     */
    public static function add($user) {
        return User::insert($user);
    }

    /**
     * Consulta obtener usuario de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return Model App\Models\User
     */
    public static function getAll() {
        return User::where('delected', 0)->get();
    }

    /**
     * Consulta actualizar un usuario de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param Model App\Models\User Modelo del usuario a actualizar
     * @param array Datos a actualizar la base de datos
     * @param boolean
     */
    public static function update($user, $userToUpdate) {
        return $user->update($userToUpdate);
    }

    /**
     * Consulta realizar el apagado logico de un usuario de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return boolean
     */
    public static function delete($user) {
        return $user->update(['deleted' => 1]);
    }

    /**
     * Consulta realizar el encendido logico de un usuario de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return boolean
     */
    public static function reactive($user) {
        return $user->update(['deleted' => 0]);
    }

}
