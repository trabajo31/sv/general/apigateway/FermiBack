<?php

namespace App\Models\SV;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sesions extends Model
{

    protected $table = 'sesion';
    
    protected $fillable = [
        'sesion_id',
        'url',
    ];
    
}
