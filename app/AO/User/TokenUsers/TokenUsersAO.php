<?php

namespace App\AO\User\TokenUsers;

use App\Models\Users\TokenUser;
class TokenUsersAO
{
    /**
     * Consulta para insertar los datos de la sesión de un servicio
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param array $serviceUser usuario del servicio a buscar
     */
    public static function setToken($infoToken){
        TokenUser::insert($infoToken);
    }

    /**
     * Consulta para hacer el apagado logico todas las sesiones en base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param array $serviceUser usuario del servicio a buscar
     */
    public static function disableAllTokens($infoToken, $update){
        TokenUser::where('user_id', $infoToken['user'])
                    ->update($update);
    }

    /**
     * Consulta verificar que exista una sesión activa de una sesión en base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param array $infoToken
     */
    public static function countLoggedRegister($infoToken){
        return TokenUser::where('user_ip', $infoToken['user_ip'])
                            ->where('token', $infoToken['token'])
                            ->where('user_id', $infoToken['user'])
                            ->where('delected', 0)
                            ->count();
    }

    /**
     * Consulta para hacer el apagado logico de una sesión en base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param array $infoToken
     */
    public static function disableToken($token){
        $objToken['state'] = 0;
        TokenUser::where('token', $token)
                    ->update($objToken);
    }


}