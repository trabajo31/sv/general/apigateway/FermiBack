<?php

namespace App\Models\SV;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicioVirtual extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_service ',
        'name_servicio',
        'service_user',
        'password',
        'service_strategy',
        'id_client',
        'id_user_create ',
        'id_user_edit',
        'delected',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function client() {
        return $this->belongsTo(Cliente::class, "id_client");
    }

    public function user_create() {
        return $this->belongsTo(User::class, "id_user");
    }
    
    public function user_edit() {
        return $this->belongsTo(User::class, "id_user_edit");
    }

}