<?php

namespace App\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class ClientsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:200|regex:/^[\pL\s.0-9]*$/u',
        ];
    }

    public function messages()
    {
        
        return [
            'name.required' => 'El nombre es obligatorio.',
            'name.max'      => 'El nombre no debe superar 200 caracteres.',
            'name.regex'    => 'El nombre debe ser texto',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status' => 500,
            'message' => $validator->errors()->all()
        ], 200));
    }

}
