<?php

namespace App\Models\SV;

use App\Models\SV\Cliente;
use App\Models\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class VirtualService extends Authenticatable implements JWTSubject
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_service',
        'subject',
        'name_servicio',
        'service_user',
        'password',
        'service_strategy',
        'id_client',
        'id_user_create',
        'id_user_edit',
        'delected',
    ];

    protected $hidden = [
        // 'password',
        'remember_token',
    ];

    public function client() {
        return $this->belongsTo(Cliente::class, "id_client");
    }

    public function user_create() {
        return $this->belongsTo(User::class, "id_user_create");
    }
    
    public function user_edit() {
        return $this->belongsTo(User::class, "id_user_edit");
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }   

}