<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SV\VirtualService;
use App\Models\User;

class ActivityLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'subject',
        'event',
        'time',
        'ip_usuario',
        'id_user',
        'id_service',
    ];

    public function user_create() {
        return $this->belongsTo(User::class, "id_user");
    }
    
    public function user_edit() {
        return $this->belongsTo(VirtualService::class, "id_service");
    }

}

