<?php

namespace App\BL\User;

use Carbon\Carbon;
use App\AO\User\UserAO;
use App\AO\Area\AreaAO;
use App\Import\UsersImport;
use App\Http\Controllers\Generic\ResponseController;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Helpers\TokenUser\JwtAuthHelperUser;

class UserBL {

    private static $response = [];
    private static $excepcion = ['msn' => 'Error al consultar en la Base de Datos', 'status' => 500];


    public static function authLogin($request) {
        
        try { 

            $ipClient = request()->ip();
            $credentials = [
                'user_network' => $request->user_network,
                'password' => $request->password
            ];
            $userNetwork = $credentials['user_network'];
            $validateUser = self::getUserByUserNetwork($userNetwork);
            if (!empty($validateUser)) {

                if ($validateUser->delected == 0) {
                    $lpad = false;
                    // $lpad = self::lpadAuthenticate($credentials['user_network'], $credentials['password']);
                    $lpad = true;

                    if($lpad) {
                        $token = Auth::login($validateUser);

                        $objJwtAuthHelper = new JwtAuthHelperUser();

                        $infoToken = [
                            'user_ip' => $ipClient,
                            'token'   => $token,
                            'user'    => $validateUser->id,
                        ];

                        $objJwtAuthHelper->saveToken($infoToken);

                        $data = [
                            'msn' => 'Usuario validado',
                            'token' => $token,
                        ];

                        $codigo = 200;
                        self::$response['data'] = $data;

                    } else {
                        $message = 'Credenciales incorrectas';
                        $codigo = 412;
                    }

                } else {
                    $message = 'El usuario fue eliminado del sistema';
                    $codigo = 412;
                }
            } else {
                Log::error('function authLogin() user is not registered in the database User: ' . $credentials['user_network'] . ' - IP: ' . $ipClient);
                $message = "El usuario no se encuentra registrado en el sistema";
                $codigo = 412;
            }

            if (isset($message)) {
                self::$response['message'] = $message;
            }

            self::$response['status'] = $codigo;

        } catch (\Throwable $th) {
            self::$response = array(
                'status' => false,
                'message' => 'No se pudo comunicar con el servidor, intentelo mas tarde.',
                'codigo' => 500
            );
        }
        return self::$response;
    }

    public static function lpadAuthenticate($user, $password)
    {
        $validated = false;
        try {
            $ldap = env('LDAP_SERVER');
            $sufixLdap = env('LDAP_SUFIX');
            $username = $user;
            $usr = $username . $sufixLdap;
            $ds = ldap_connect($ldap);
            ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
            $validated = ldap_bind($ds, $usr, $password);
        } catch (\Exception $e) {
            Log::error('function lpadAuthenticate() Error autentication LDAP' . $e->getMessage());
            $validated = false;
            $validated = true;
        }
        return $validated;
    }

    /**
     * Obtener un usuario por el nombre de usuario
     * Method to login a virtual service
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public static function getUserByUserNetwork($userNetwork) {

        try {

            return $objData = UserAO::getUserByUserNetwork($userNetwork);
            self::$response = ['data' => $objData, 'status' => 200];
            Log::info("Successful consultation -> function getUserByName()");
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function getUserByName()");
        }
    }


    public static function add($data) {
        try {
            $userNetwork = $data['user_network'];
            
            $data_user = UserAO::getUserByUserNetwork($userNetwork);
            
            if($data_user){
                self::$response = ['msn' => 'El usuario existe en el sistema', 'status' => 400];
            }else{
                $now = Carbon::now('UTC')->subHours(5)->toDateTimeString();
                $save = [
                    'name' => $data['name'],
                    'user_network' => $data['user_network'],
                    'id_rol' => 1,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
                $user = UserAO::add($save);
                self::$response = ['data' => $user, 'msn' => 'Usuario creado!', 'status' => 200];
                Log::info("Successful consultation -> function add()");
            }

        } catch (\Throwable $th) {
            dd($th);
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function add()");
        }
        return self::$response;
    }

    /**
     * Metodo para obtener todos los usuarios
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return Response 
     */
    public static function getAll() {
        try {
            $objData = UserAO::getAll();
            self::$response = ['data' => $objData, 'msn' => 'Exito', 'status' => 200];
            Log::info("Record deleted -> function getAll()");
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function getAll()");
        }
        return self::$response;
    }

    public static function update($data) {
        try {
            $data_user = UserAO::getUserById($data['id']);
            if($data_user){
                $objData = UserAO::update($data_user, $data);
                self::$response = ['data' => $objData, 'msn' => 'Usuario actualizado!', 'status' => 200];
            }else{
                self::$response = ['msn' => 'No se pudo actualizar', 'status' => 400];

            }
            Log::info("Record updated -> function update()");
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function update()");
        }
        return ResponseController::objectResponse(self::$response);
    }


    public static function delete($data) {
        try {
            $virtualService = UserAO::getUserById($data);            
            if($virtualService){
                $objData = UserAO::delete($virtualService);
                if($objData) {
                    self::$response = ['data' => $objData ,'msn' => 'El usuario fue eliminado', 'status' => 200];
                    Log::info("Record updated -> function delete()");    
                } else {
                    self::$response = ['data' => 'El usuario no pudo ser eliminado', 'status' => 200];
                    Log::info("Record updated -> function update()");   
                }
            } else {
                self::$response = ['msn' => 'El usuario no se encuentra registrado o está eliminado', 'status' => 400];
                Log::info("Record updated -> function delete()");
            }
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function delete()");
        }
        return ResponseController::objectResponse(self::$response);
    }


}
