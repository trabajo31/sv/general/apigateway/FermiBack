<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:200|regex:/^[\pL\s.0-9]*$/u',
            'user_network'  => 'required|max:100|regex:/^[\pL\s.0-9]*$/u',
            'id_rol'        => 'required|max:100|numeric',
        ];
    }

    public function messages()
    {
        
        return [
            'name.required'         => 'El nombre es obligatorio.',
            'name.max'              => 'El nombre no debe superar 200 caracteres.',
            'user_network.required' => 'El Usuario de red es obligatorio.',
            'user_network.max'      => 'El Usuario de red  es de maximo 45 caracteres',
            'user_network.regex'    => 'El campo Usuario de red debe ser texto',
            'password.required'     => 'El Nombre es obligatoria.',
            'password.max'          => 'El campo Nombre de red es de maximo 50 caracteres',
            'password.regex'        => 'El campo Nombre de red debe ser solo letras',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status' => 500,
            'message' => $validator->errors()->all()
        ], 200));
    }
}
