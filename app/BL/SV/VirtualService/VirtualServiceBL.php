<?php

namespace App\BL\SV\VirtualService;

use App\AO\SV\VirtualServiceAO;
use App\Http\Controllers\Generic\ResponseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\CurrentUser\CurrentUser;

class VirtualServiceBL {

    private static $response = [];
    private static $excepcion = ['msm' => 'Error al consultar en la Base de Datos', 'status' => 500];

    public static function add($data) {
        try {
            $user = $data['service_user'];
            $data_user = VirtualServiceAO::getServiceByUserService($user);
            if(!$data_user){
                $data['password'] = Crypt::encryptString($data['password']);
                $currentUserId = CurrentUser::getCurrentUser()->id;
                $data['id_user_edit'] = $currentUserId;
                $data['id_user_create'] = $currentUserId;
                $result = VirtualServiceAO::add($data);
                self::$response = ['data' => $result,'msn' => 'Servicio virtual creado!', 'status' => 200];
                Log::info("Successful consultation -> function add()");
            }else{
                self::$response = ['data' => $data_user->id,'status' => 202, 'msn' => 'El servicio ya existe en el sistema'];
            }

        } catch (\Throwable $th) {
            dd($th);
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function add()");
        }
        return self::$response;
    }

    public static function update($data) {
        try {
            $data_user = VirtualServiceAO::getServiceById($data['id']);
            if($data_user){
                $currentUser = CurrentUser::getCurrentUser();
                $data['id_user_edit'] = $currentUser->id;
                $data['password'] = Crypt::encryptString($data['password']);
                $objData = VirtualServiceAO::update($data_user, $data);
                if($objData) {
                    self::$response = ['data' => $objData, 'msn' => 'El servicio fue actualizado', 'status' => 200];
                    Log::info("Record updated -> function update()");   
                } else {
                    self::$response = ['data' => 'El servicio no pudo ser actualizado', 'status' => 200];
                    Log::info("Record updated -> function update()");   
                }
          
            }else{
                $arrResponse = array('mesaje' => 'El servicio no se encuentra registrado en el sistema');
                self::$response = ['data' => $arrResponse, 'status' => 200];
            }
        } catch (\Throwable $th) {
            dd($th);
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function update()");
        }
        return self::$response;
    }

    public static function delete($data) {
        try {
            $virtualService = VirtualServiceAO::getServiceById($data);            
            if($virtualService){
                $objData = VirtualServiceAO::delete($virtualService);
                if($objData) {
                    self::$response = ['data' => $objData ,'msn' => 'El servicio fue eliminado', 'status' => 200];
                    Log::info("Record updated -> function delete()");    
                } else {
                    self::$response = ['data' => 'El servicio no pudo ser eliminado', 'status' => 200];
                    Log::info("Record updated -> function update()");   
                }
            } else {
                self::$response = ['msn' => 'El servicio virtual no se encuentra registrado o está eliminado', 'status' => 400];
                Log::info("Record updated -> function delete()");
            }
        } catch (\Throwable $th) {
            dd($th);
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function delete()");
        }
        return self::$response;
    }

    public static function reactive($data) {
        try {
            $virtualService = VirtualServiceAO::getServiceDeletedById($data);            
            if($virtualService){
                $objData = VirtualServiceAO::reactive($virtualService);
                if($objData) {
                    self::$response = ['data' => $objData ,'msn' => 'El servicio fue reactivado', 'status' => 200];
                    Log::info("Record updated -> function reactive()");    
                } else {
                    self::$response = ['data' => 'El servicio no pudo ser reactivado', 'status' => 200];
                    Log::info("Record updated -> function reactive()");   
                }
            } else {
                self::$response = ['msn' => 'El servicio virtual no se encuentra registrado', 'status' => 400];
                Log::info("Record updated -> function reactive()");
            }
        } catch (\Throwable $th) {
            dd($th);
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function reactive()");
        }
        return self::$response;
    }

    public static function getAll() {
        try {
            $objData = VirtualServiceAO::getAll();
            foreach($objData as $data) {
                $data->password = Crypt::decryptString($data->password);
            }
            self::$response = ['data' => $objData, 'msn' => 'Exito', 'status' => 200];
            Log::info("Record deleted -> function getAll()");
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function getAll()");
        }
        return self::$response;
    }

    public static function getAllusersSV() {
        try {
            $objData = VirtualServiceAO::getAllusersSV();
            self::$response = ['data' => $objData, 'msn' => 'Exito', 'status' => 200];
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function getAll()");
        }
        return self::$response;
    }

    public static function getServiceById($id) {
        try {
            $objData = VirtualServiceAO::getServiceById($id);
            self::$response = ['data' => $objData, 'msn' => 'Exito', 'status' => 200];
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
            Log::error($th->getMessage()." function getServiceById()");
        }
        return self::$response;
    }

}
