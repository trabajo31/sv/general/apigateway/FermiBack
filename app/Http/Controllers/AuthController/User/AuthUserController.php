<?php

namespace App\Http\Controllers\AuthController\User;


use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\BL\SV\VirtualService\VirtualServiceBL;
use App\BL\User\UserBL;
use App\Http\Requests\SV\LoginVirtualServiceRequest;
use App\Http\Requests\User\UserLoginRequest;
use Illuminate\Http\Request;

/** 
 * Controlador para la utenticación de un servicio virtual
 * 
 * 
 * @package App\Http\Controllers\AuthController\SV
 * @author Luis Manuel Hernandez Jimenez (luis.hernandez@grupokonecta.com)
 * @date 01/01/2016
 * */
class AuthUserController extends Controller
{

    public function __construct()
    {   
        $this->middleware('auth:api', ['except' => ['login', 'register', 'me', ['addRol']]]);
    }

    /**
     * Metodo para el login de un servicio virtual
     * Method to login a virtual service
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description En esta funcion se hace el login del servicio virtual
     * Pasando como parametro service_user y el password
     * @param array $requestHttp con el service_user y el password
     * @return Data $token
     */
    public function login(UserLoginRequest $request)
    {        
        return UserBL::authLogin($request);
    }

    /**
     * Metodo que devuelve el token del usuario actual
     * Method to login a virtual service
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return JWT $
     */
    public function me()
    {
        return ['status' => 200, 'data' => JWTAuth::parseToken()->authenticate()];
    }

    /**
     * Metodo que devuelve el token del usuario actual
     * Method to login a virtual service
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @return response $
     */
    public function logout()
    {
        auth()->logout();
        
        return response()->json(['message' => 'Successfully logged out']);
    }


}