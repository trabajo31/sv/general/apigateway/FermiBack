<?php

namespace App\Helpers\CurrentDateTime;

use Carbon\Carbon;

class CurrentDateTime
{
    public static function getCurrentDate() {
        return Carbon::now('UTC')->subHours(5)->toDateTimeString();
    }
}
