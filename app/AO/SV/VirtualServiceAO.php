<?php

namespace App\AO\SV;

use App\Models\SV\VirtualService;

class VirtualServiceAO
{
    /**
     * Consulta para obtener un servicio por nombre de usuario de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param string $serviceUser usuario del servicio a buscar
     * @return Modelo App\Models\SV\VirtualService
     */
    public static function getServiceByUserService($serviceUser) {
        return VirtualService::where('service_user', $serviceUser)->get()->first();
    }

    /**
     * Consulta para insertar un servicio en la base de datos
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @param array $objRequest datos a insertar
     * @return true
     */
    public static function add($objRequest) {
        $user = VirtualService::create($objRequest);
        return $user->id;
    }

    /**
     * Consulta para obtenert todos los servicios 
     * virtuales de la base de datos
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @return Modelo App\Models\SV\VirtualService
     */
    public static function getAll() {
        return VirtualService::where('delected', 0)->with(['client', 'user_create', 'user_edit'])->get();
    }

    /**
     * Consulta para obtener todos los usuarios de los 
     * servicios virtuales de la base de datos
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @return Modelo App\Models\SV\VirtualService
     */
    public static function getAllusersSV() {
        return VirtualService::select('service_user')->where('delected', 0)->get();
    }

    /**
     * Consulta para obtener un servicio virtual de la base de datos
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param int $id ID del cliente
     * @return Modelo App\Models\SV\VirtualService
     */
    public static function getServiceById($id) {
        return VirtualService::where('id', $id)->where('delected', 0)->with(['client', 'user_create', 'user_edit'])->first();
    }

    /**
     * Consulta para obtener un servicio virtual de la base de datos
     * Que se encuentre eliminado
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param int $id ID del cliente
     * @return Modelo App\Models\SV\VirtualService
     */
    public static function getServiceDeletedById($id) {
        return VirtualService::where('id', $id)->where('delected', 1)->with(['client', 'user_create', 'user_edit'])->first();
    }

    /**
     * Consulta para obtener un servicio virtual de la base de datos
     * Que se encuentre eliminado
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param Modelo $virtualService App\Models\SV\VirtualService
     * @param aray $virtualServiceToUpdate array con los datos a actualizar
     * @return boolean
     */
    public static function update($virtualService, $virtualServiceToUpdate) {
        return $virtualService->update($virtualServiceToUpdate);
    }

    /**
     * Consulta para realizar el apagado logico de un servicio virtual
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param Modelo $virtualService App\Models\SV\VirtualService
     * @param aray $virtualService id del servicio virtual
     * @return boolean
     */
    public static function delete($virtualService) {
        return $virtualService->update(['delected' => 1]);
    }

    /**
     * Consulta para realizar el encendido logico de un servicio virtual
     * 
     * @date 04/01/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param Modelo $virtualService App\Models\SV\VirtualService
     * @return boolean
     */
    public static function reactive($virtualService) {
        return $virtualService->update(['delected' => 0]);
    }

}
