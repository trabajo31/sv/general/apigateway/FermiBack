
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `auth_sv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activitylogs`
--

CREATE TABLE `activitylogs` (
  `id` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `event` text NOT NULL,
  `response` varchar(1024) NOT NULL,
  `time` date NOT NULL,
  `ip_usuario` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_client` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_user_create` int(11) NOT NULL,
  `id_user_edit` int(11) NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_client`, `name`, `id_user_create`, `id_user_edit`, `delected`) VALUES
(1, 'Prueba Konecta', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL COMMENT 'Id del rol',
  `rol_name` varchar(100) DEFAULT NULL COMMENT 'name del rol'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol_name`) VALUES
(1, 'Administrador'),
(2, 'User');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE `sesion` (
  `id` int(100) NOT NULL,
  `sesion_id` varchar(11) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token_sv`
--

CREATE TABLE `token_sv` (
  `id` int(20) NOT NULL,
  `user_ip` varchar(100) NOT NULL,
  `service_id` int(100) NOT NULL,
  `token` longtext NOT NULL,
  `locked` int(1) NOT NULL,
  `date_init` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token_users`
--

CREATE TABLE `token_users` (
  `id` int(11) NOT NULL,
  `user_ip` varchar(100) NOT NULL,
  `token` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Id del usuario',
  `name` varchar(124) NOT NULL COMMENT 'name del usuario',
  `user_network` varchar(100) NOT NULL COMMENT 'Usuario de red',
  `id_rol` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `user_network`, `id_rol`, `created_at`, `updated_at`) VALUES
(1, 'Luis Hernandez', 'luis.hernandez.ji', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `virtual_services`
--

CREATE TABLE `virtual_services` (
  `id` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `name_servicio` varchar(100) NOT NULL,
  `service_user` varchar(100) NOT NULL,
  `password` longtext NOT NULL,
  `service_strategy` varchar(253) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user_create` int(11) NOT NULL,
  `id_user_edit` int(11) NOT NULL,
  `delected` int(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `virtual_services`
--

INSERT INTO `virtual_services` (`id`, `name_servicio`, `service_user`, `password`, `service_strategy`, `id_client`, `id_user_create`, `id_user_edit`, `delected`, `updated_at`, `created_at`) VALUES
(1, 'Prueba Konecta', 'prueba', 'prueba123', 'https://widget.grupokonecta.co/genesys/2/chat/allus', 1, 1, 1, 0, '2021-12-07', '2021-12-07');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_service` (`id_service`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_user_2` (`id_user`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `id_user_create` (`id_user_create`),
  ADD KEY `id_user_edit` (`id_user_edit`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `token_sv`
--
ALTER TABLE `token_sv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `service_id_2` (`service_id`);

--
-- Indices de la tabla `token_users`
--
ALTER TABLE `token_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `virtual_services`
--
ALTER TABLE `virtual_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_user_create` (`id_user_create`),
  ADD KEY `id_user_edit` (`id_user_edit`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del rol', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sesion`
--
ALTER TABLE `sesion`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `token_sv`
--
ALTER TABLE `token_sv`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT de la tabla `token_users`
--
ALTER TABLE `token_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del usuario', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `virtual_services`
--
ALTER TABLE `virtual_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  ADD CONSTRAINT `activitylogs_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `activitylogs_ibfk_2` FOREIGN KEY (`id_service`) REFERENCES `virtual_services` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`id_user_create`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`id_user_edit`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `token_sv`
--
ALTER TABLE `token_sv`
  ADD CONSTRAINT `token_sv_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `virtual_services` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);

--
-- Filtros para la tabla `virtual_services`
--
ALTER TABLE `virtual_services`
  ADD CONSTRAINT `virtual_services_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clientes` (`id_client`) ON UPDATE CASCADE,
  ADD CONSTRAINT `virtual_services_ibfk_4` FOREIGN KEY (`id_user_create`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `virtual_services_ibfk_5` FOREIGN KEY (`id_user_edit`) REFERENCES `users` (`id`);

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `delect_sesion` ON SCHEDULE EVERY 1 MINUTE STARTS '2022-01-07 13:35:26' ENDS '2026-07-31 23:59:59' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE token_sv SET token_sv.delected = 1 WHERE UNIX_TIMESTAMP(token_sv.date_init) < (UNIX_TIMESTAMP()-0.5*3600)$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
