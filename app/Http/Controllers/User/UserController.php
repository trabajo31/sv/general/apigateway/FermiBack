<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SV\VirtualServiceRequest;
use App\BL\User\UserBL;
use App\Http\Controllers\Generic\ResponseController;

/** 
 * Controlador para las gestiones de los servicios virtuales
 * 
 * 
 * @package App\Http\Controllers\SV
 * @author Luis Manuel Hernandez Jimenez (luis.hernandez@grupokonecta.com)
 * @date 01/01/2016
 * */
class UserController extends Controller
{

    /**
     * Metodo para registrar un servicio virtual
     * Method to login a virtual service
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param VirtualServiceRequest $request
     * @return json $response
     */
    public function addUser(Request $request) {
        $objRequest = $request->all();
        $response = UserBL::add($objRequest);
        return ResponseController::objectResponse($response, 200);
    }

    public function update(VirtualServiceRequest $request) {
        $objRequest = $request->all();
        $response = UserBL::update($objRequest);
        return response()->json($response, 200);

    }

    public function delete() {
        $objRequest = request()->get('id');
        $response = UserBL::delete($objRequest);
        return response()->json($response, 200);
    }

    /**
     * Controlador para obtener el listado de usuarios
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function getAll() {
        $response = UserBL::getAll();
        return ResponseController::objectResponse($response);
    }

}
