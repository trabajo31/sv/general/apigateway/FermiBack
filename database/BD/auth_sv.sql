
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `auth_sv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activitylogs`
--

CREATE TABLE `activitylogs` (
  `id` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `event` text NOT NULL,
  `response` varchar(1024) NOT NULL,
  `time` date NOT NULL,
  `ip_usuario` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_user_create` int(11) NOT NULL,
  `id_user_edit` int(11) NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `id_user_create`, `id_user_edit`, `delected`) VALUES
(1, 'Prueba Konecta', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL COMMENT 'Id del rol',
  `rol_name` varchar(100) DEFAULT NULL COMMENT 'name del rol'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `rol_name`) VALUES
(1, 'Administrador'),
(2, 'User');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE `sesion` (
  `id` int(100) NOT NULL,
  `sesion_id` varchar(11) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sesion`
--

INSERT INTO `sesion` (`id`, `sesion_id`, `url`) VALUES
(0, '[value-2]', '1641668035');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token_sv`
--

CREATE TABLE `token_sv` (
  `id` int(20) NOT NULL,
  `user_ip` varchar(100) NOT NULL,
  `service_id` int(100) NOT NULL,
  `token` longtext NOT NULL,
  `locked` int(1) NOT NULL,
  `date_init` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `token_sv`
--

INSERT INTO `token_sv` (`id`, `user_ip`, `service_id`, `token`, `locked`, `date_init`, `updated_at`, `delected`) VALUES
(194, '127.0.0.1', 12, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblNlcnZpY2UiLCJpYXQiOjE2NDE2Njk2MTksImV4cCI6MTY0MTY3MzIxOSwibmJmIjoxNjQxNjY5NjE5LCJqdGkiOiJvUnpKN1pSVUhIYjFna0pCIiwic3ViIjoxMiwicHJ2IjoiNmRhZjMyZDk3ZWQzYmMwNjU1ZjM4MjYxM2U4OWZhYTVhYjFlODYxNiJ9.1zHNpni9d3XV1BD_5ukcPpBY4UsHNLlrEItndiB5HWo', 0, '2022-01-08 14:50:26', '2022-01-08 14:20:19', 1),
(195, '127.0.0.1', 12, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblNlcnZpY2UiLCJpYXQiOjE2NDE2NzU3MjksImV4cCI6MTY0MTY3OTMyOSwibmJmIjoxNjQxNjc1NzI5LCJqdGkiOiJueTVwSVJ3U3hiWVJVRmVJIiwic3ViIjoxMiwicHJ2IjoiNmRhZjMyZDk3ZWQzYmMwNjU1ZjM4MjYxM2U4OWZhYTVhYjFlODYxNiJ9.XvW1000Ry-bLBozf8c-EtM59oQnnjOtWjVYFzpe2uS0', 0, '2022-01-08 16:32:26', '2022-01-08 16:02:09', 1),
(196, '127.0.0.1', 12, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblNlcnZpY2UiLCJpYXQiOjE2NDE2NzczMjUsImV4cCI6MTY0MTY4MDkyNSwibmJmIjoxNjQxNjc3MzI1LCJqdGkiOiJ6VUt4UVl5WGQ5Wk9BMDB2Iiwic3ViIjoxMiwicHJ2IjoiNmRhZjMyZDk3ZWQzYmMwNjU1ZjM4MjYxM2U4OWZhYTVhYjFlODYxNiJ9.OWAYE0JRraQAo0eT2E0-aGVzZdnH4n3l8ygrTIsQzAU', 0, '2022-01-08 16:59:26', '2022-01-08 16:28:45', 1),
(197, '127.0.0.1', 12, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblNlcnZpY2UiLCJpYXQiOjE2NDE2NzczODIsImV4cCI6MTY0MTY4MDk4MiwibmJmIjoxNjQxNjc3MzgyLCJqdGkiOiJhZENhOEQyd21mcGE5ZG9UIiwic3ViIjoxMiwicHJ2IjoiNmRhZjMyZDk3ZWQzYmMwNjU1ZjM4MjYxM2U4OWZhYTVhYjFlODYxNiJ9.wu3khUW9o4dLZ5-L-kSimWKoTT5M6rFt3AnPWcfOuzk', 0, '2022-01-08 17:00:26', '2022-01-08 16:29:42', 1),
(198, '127.0.0.1', 12, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblNlcnZpY2UiLCJpYXQiOjE2NDE2NzczODYsImV4cCI6MTY0MTY4MDk4NiwibmJmIjoxNjQxNjc3Mzg2LCJqdGkiOiJ1Y3NSU1lNTG9kMWlQRDRmIiwic3ViIjoxMiwicHJ2IjoiNmRhZjMyZDk3ZWQzYmMwNjU1ZjM4MjYxM2U4OWZhYTVhYjFlODYxNiJ9.RsZjLLAlY6K1obhjB-uNKc5taHq_3iIhMkVWAw6qHP0', 0, '2022-01-08 17:00:26', '2022-01-08 16:29:46', 1),
(199, '127.0.0.1', 12, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblNlcnZpY2UiLCJpYXQiOjE2NDE2Nzc3OTksImV4cCI6MTY0MTY4MTM5OSwibmJmIjoxNjQxNjc3Nzk5LCJqdGkiOiJ0VzVMa2VKR1VmcnFiOFBuIiwic3ViIjoxMiwicHJ2IjoiNmRhZjMyZDk3ZWQzYmMwNjU1ZjM4MjYxM2U4OWZhYTVhYjFlODYxNiJ9.nZpkgg--0ucM7Fw6rJwz6sm3ggZSFX5hiVntH7Uc63g', 0, '2022-01-08 17:07:26', '2022-01-08 16:36:39', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token_users`
--

CREATE TABLE `token_users` (
  `id` int(11) NOT NULL,
  `user_ip` varchar(100) NOT NULL,
  `token` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `token_users`
--

INSERT INTO `token_users` (`id`, `user_ip`, `token`, `user_id`, `date`, `delected`) VALUES
(44, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblVzZXIiLCJpYXQiOjE2NDE2ODMwNTAsImV4cCI6MTY0MTY4NjY1MCwibmJmIjoxNjQxNjgzMDUwLCJqdGkiOiJKNDJDZ0FuUGZuV25nbkJYIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.-Vvyyjj5pP7rxNY6SETdUAfKwSLKg3sS0-onrYiBbJI', 1, '2022-01-08 00:00:00', 0),
(45, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpblVzZXIiLCJpYXQiOjE2NDE2ODgxODAsImV4cCI6MTY0MTY5MTc4MCwibmJmIjoxNjQxNjg4MTgwLCJqdGkiOiJyZTFkZ0lvNFRSQUl2VGs5Iiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.lea0rBjybigWbeb9W2RssK0qwYihvs16DNw5pc5X9zw', 1, '2022-01-08 19:29:40', 0),
(46, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODE5MiwiZXhwIjoxNjQxNjkxNzkyLCJuYmYiOjE2NDE2ODgxOTIsImp0aSI6ImRXOEFQbDRnd1A3djVJWUsiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.79aN0DB_LdCCKeaHNOOha9QgDOuHEGbF4YF2nhhGlms', 1, '2022-01-08 19:29:52', 0),
(47, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODE5MywiZXhwIjoxNjQxNjkxNzkzLCJuYmYiOjE2NDE2ODgxOTMsImp0aSI6InBWSDkxZVhPbjRZY0x2akgiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.qwWp6DXMwRguoywdmjMyRmqz3geuWz5_l6GnL3QKdRU', 1, '2022-01-08 19:29:53', 0),
(48, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODIwNCwiZXhwIjoxNjQxNjkxODA0LCJuYmYiOjE2NDE2ODgyMDQsImp0aSI6Ilk0RlFXZHBwbnJ3ejlWbVkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.CL1awTK7jmkBHRgzG9s_9BPR3mQrx5p_QWEpHbIKI2s', 1, '2022-01-08 19:30:04', 0),
(49, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODI1MSwiZXhwIjoxNjQxNjkxODUxLCJuYmYiOjE2NDE2ODgyNTEsImp0aSI6IlpROWsyd2lVaXNWTkt6RmIiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.nlpnbLQ9s8qAeM1X2Vk2FEGXWa_NXp5MSukiii7cA24', 1, '2022-01-08 19:30:51', 0),
(50, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODI4MiwiZXhwIjoxNjQxNjkxODgyLCJuYmYiOjE2NDE2ODgyODIsImp0aSI6ImNGRWdSTU0wVjU2bkpHUmQiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.jfOcC3lMEYZNQXnHaMOU7F9MtP4IQQixwM47IOVOmb4', 1, '2022-01-08 19:31:22', 0),
(51, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODMwMSwiZXhwIjoxNjQxNjkxOTAxLCJuYmYiOjE2NDE2ODgzMDEsImp0aSI6IkdjbjBxaTNwQzEwc2tCSk0iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.J7jiT8XeV8A5A-o8sNzipJgAvs48LLRU4Z8GqtGsNyk', 1, '2022-01-08 19:31:41', 0),
(52, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODMxNywiZXhwIjoxNjQxNjkxOTE3LCJuYmYiOjE2NDE2ODgzMTcsImp0aSI6IlBPUFNqOUJZbE04Ulo1dVgiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.2hA2knBuJMHrh3-WSr66FN_uKHEkDONRm0lS08OgkNo', 1, '2022-01-08 19:31:57', 0),
(53, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODMyOCwiZXhwIjoxNjQxNjkxOTI4LCJuYmYiOjE2NDE2ODgzMjgsImp0aSI6ImRZMG9TZWU2eW56SU9iSk4iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.MUZ6VlEoK2nGc9kxsdCbC-07iPWF4O316EIwZ1Z7Ffc', 1, '2022-01-08 19:32:08', 0),
(54, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODM3MCwiZXhwIjoxNjQxNjkxOTcwLCJuYmYiOjE2NDE2ODgzNzAsImp0aSI6InFtQzFDWmlZcVVYZlhtZHEiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.2Op2l8Gq53ke0mBB7O7YcsE6sMPfYEopiYRZ3EBG2W4', 1, '2022-01-08 19:32:50', 0),
(55, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTY4ODQwMiwiZXhwIjoxNjQxNjkyMDAyLCJuYmYiOjE2NDE2ODg0MDIsImp0aSI6IkxlVXVsblBFTVQwVzVXZTIiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.6Ut3THQ-Eqy1qR6B5ibD59GnOE0mUgjD3COHy65bhUo', 1, '2022-01-08 19:33:22', 0),
(56, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxMzE4MCwiZXhwIjoxNjQxOTE2NzgxLCJuYmYiOjE2NDE5MTMxODEsImp0aSI6IkNPak1mUzRYSHE0RW5ieFQiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.KZFdb82QixaG6VXCEYg7X2CJ7QwIwma9fuBIrU84fcw', 1, '2022-01-11 09:59:41', 0),
(57, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxMzQ0MCwiZXhwIjoxNjQxOTE3MDQwLCJuYmYiOjE2NDE5MTM0NDAsImp0aSI6IlRKeXdxSlgzNUJ1T2xqbEkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.hkUKDHVeAuxDJqQ8RcRunhOJI-e0rKmUe7ZGlO49V1w', 1, '2022-01-11 10:04:00', 0),
(58, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxNTE2NCwiZXhwIjoxNjQxOTE4NzY0LCJuYmYiOjE2NDE5MTUxNjQsImp0aSI6InVKYzFHdkNacVVmZ3FTT3UiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.qkG2rh4LZ5eiCqHyEeyeBAcfFKiGgvHETRcTkOdVr5k', 1, '2022-01-11 10:32:44', 0),
(59, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxNTMwMiwiZXhwIjoxNjQxOTE4OTAyLCJuYmYiOjE2NDE5MTUzMDIsImp0aSI6IkplTjNndUJIY1N2MnBEM3kiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4TtB-7Sm5Lbim7uy9izkE8TatNQoYAscg8R_ZHogvx4', 1, '2022-01-11 10:35:02', 0),
(60, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxNzIxMywiZXhwIjoxNjQxOTIwODEzLCJuYmYiOjE2NDE5MTcyMTMsImp0aSI6InVzNFlTVUJUM2tUWEpRN00iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.-iDfmd80rlcklDCLYlyJFeNcgQmOmzBoRaJWNXpIQ9w', 1, '2022-01-11 11:06:53', 0),
(61, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxNzMxNiwiZXhwIjoxNjQxOTIwOTE2LCJuYmYiOjE2NDE5MTczMTYsImp0aSI6ImFvanQwY1Y5U0dYWGNmWHIiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.No0GUB2DhcPJPfu91K7kxDxgZlUniFqNqFqbHM_WjeI', 1, '2022-01-11 11:08:36', 0),
(62, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxNzMyNywiZXhwIjoxNjQxOTIwOTI3LCJuYmYiOjE2NDE5MTczMjcsImp0aSI6InA3ZlpmV2t6MWRtb01oU08iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.YG48DRB_f1ZofbXqsqONiNS-sWAX22Ho7dXe53lNbfk', 1, '2022-01-11 11:08:47', 0),
(63, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxNzQxMCwiZXhwIjoxNjQxOTIxMDEwLCJuYmYiOjE2NDE5MTc0MTAsImp0aSI6Ik93bkJBYW9RWlhKckxWZFEiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.p_d3T8HutZXhi3pT3Ugw8R0EqQbzNzD2MsG_YQFqF2o', 1, '2022-01-11 11:10:10', 0),
(64, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxODQ0MywiZXhwIjoxNjQxOTIyMDQzLCJuYmYiOjE2NDE5MTg0NDMsImp0aSI6IlFjbnNCMVZQNkM2ZEtWUmoiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.ib35wSFyKlttFF7_rOHhhr4Se6Qm95TtjAyX2LM2i1Y', 1, '2022-01-11 11:27:23', 0),
(65, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkxOTAwNiwiZXhwIjoxNjQxOTIyNjA2LCJuYmYiOjE2NDE5MTkwMDYsImp0aSI6ImViQUNDRW1JSTZnQ0dxU0wiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.KKOMEyERDUAei-GpUJQzYcZzutevYf7D6JK0KGJf46g', 1, '2022-01-11 11:36:46', 0),
(66, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTkyMjA5MywiZXhwIjoxNjQxOTI1NjkzLCJuYmYiOjE2NDE5MjIwOTMsImp0aSI6InluYWdTTUdBbTlTMEc1dU8iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.qL0PuKDNRSSXHhLio1wvj_BtnkSp8XXqUME_z4yZMC8', 1, '2022-01-11 12:28:13', 0),
(67, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTk5MDk0OSwiZXhwIjoxNjQxOTk0NTQ5LCJuYmYiOjE2NDE5OTA5NDksImp0aSI6Inc0eTkzQk45cnhOSFphZnkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ._2j6hAPHKUCaTiLoR5fKjTGYIes4Vf4ux1H6egbx3pA', 1, '2022-01-12 07:35:49', 0),
(68, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MTk5NDEyMywiZXhwIjoxNjQxOTk3NzIzLCJuYmYiOjE2NDE5OTQxMjMsImp0aSI6ImFFS0w0bHVrRVRQcnJkZUQiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4h7My01kAWC6OoUhx_DkTDInM4XRmlCvZC5dTZSEfsY', 1, '2022-01-12 08:28:43', 0),
(69, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwMDEzMCwiZXhwIjoxNjQyMDAzNzMwLCJuYmYiOjE2NDIwMDAxMzAsImp0aSI6IjExMThUb2g2OGluNTNuNHUiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.BzAxm5KocumVqAgmChSEOrtE72Kox3TICbjmBnpkIWI', 1, '2022-01-12 10:08:51', 0),
(70, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwMDE0MiwiZXhwIjoxNjQyMDAzNzQyLCJuYmYiOjE2NDIwMDAxNDIsImp0aSI6Ijd1YmFXMk5WWkxyZUtCYmwiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.UBU-IEca9ryCi3AzhowcWMmJRdm5bhHT5FDn9KT7eO8', 1, '2022-01-12 10:09:02', 0),
(71, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwMDI5MCwiZXhwIjoxNjQyMDAzODkwLCJuYmYiOjE2NDIwMDAyOTAsImp0aSI6ImpQalZ3U1Y4SnhSR1JtUVgiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.2L4jAHObWhUTPSpld4QbidTYQeV4dWNjnLYGtYeaR-0', 1, '2022-01-12 10:11:30', 0),
(72, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwMzIyOCwiZXhwIjoxNjQyMDA2ODI4LCJuYmYiOjE2NDIwMDMyMjgsImp0aSI6Ik5OSUZlSUVBbDd3N0VjUWQiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.jPCe4TmFz1dQWLfSlb348oWsw4fWA5ewtVAl1I80Gbc', 1, '2022-01-12 11:00:28', 0),
(73, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwNTQ0NywiZXhwIjoxNjQyMDA5MDQ3LCJuYmYiOjE2NDIwMDU0NDcsImp0aSI6ImtqZTgwdVcyRlVURWthYjkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.0BA2cgVgbhOQHYtSZC-2rqwXV1HM6eR0M1yGTFjK54o', 1, '2022-01-12 11:37:27', 0),
(74, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwNzQwNywiZXhwIjoxNjQyMDExMDA3LCJuYmYiOjE2NDIwMDc0MDcsImp0aSI6IlZLTzN2dmdtWlh0WmZrNjkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.nv0kaINtAPbtD_bhRQbJeCNG_n8LaBGRCEUcf1WJx00', 1, '2022-01-12 12:10:07', 0),
(75, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwNzQxNCwiZXhwIjoxNjQyMDExMDE0LCJuYmYiOjE2NDIwMDc0MTQsImp0aSI6Ik9KTEdMWktCcHc2S29pUVoiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.MXW9qBDu6FIz88JIcd1ngPAZ6Ljbs5i9dXBo6ibrlIA', 1, '2022-01-12 12:10:14', 0),
(76, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAwOTcyMywiZXhwIjoxNjQyMDEzMzIzLCJuYmYiOjE2NDIwMDk3MjMsImp0aSI6IlBuRFgwVjlIdnNya3dZN24iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.GV-gPZ1tcdS1Ojn9RxHtX874dlo1ZOq8MqVcTgitOys', 1, '2022-01-12 12:48:43', 0),
(77, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAxMjg2MywiZXhwIjoxNjQyMDE2NDYzLCJuYmYiOjE2NDIwMTI4NjMsImp0aSI6InlkT2dieVJyOW5lMHdCV2QiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.NAvosarDI088XFOeK-SwMlUTE6ooP9nW9whb0R1lGSM', 1, '2022-01-12 13:41:03', 0),
(78, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0MjAxMjg5NywiZXhwIjoxNjQyMDE2NDk3LCJuYmYiOjE2NDIwMTI4OTcsImp0aSI6Ik1Xc3NHclQ0dnRVZ2VmNGUiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.yT4fhf5bZhXZuVJKOYyCzkt8O884TcQXo_q934BsTJU', 1, '2022-01-12 13:41:37', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Id del usuario',
  `name` varchar(124) NOT NULL COMMENT 'name del usuario',
  `user_network` varchar(100) NOT NULL COMMENT 'Usuario de red',
  `id_rol` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `user_network`, `id_rol`, `created_at`, `updated_at`) VALUES
(1, 'L', 'luis.hernandez.ji', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'M', 'lu3232hernandez.ji', 1, '2021-12-03 00:00:00', '2021-12-03 00:00:00'),
(5, 'H', 'luis.23.ji', 1, '2021-12-03 00:00:00', '2021-12-03 00:00:00'),
(6, 'J', 'luis.hernandez.ji', 1, '2021-12-03 00:00:00', '2021-12-03 00:00:00'),
(7, 'prueba', 'luis.hernandez.jis', 1, '2022-01-08 00:00:00', '2022-01-08 00:00:00'),
(8, 'prueba', 'luis.hernandez.jiss', 1, '2022-01-08 22:07:47', '2022-01-08 22:07:47'),
(9, 'prueba', 'luis.hernandez.jisss', 1, '2022-01-08 23:19:04', '2022-01-08 23:19:04'),
(10, 'prueba', 'luis.hernandez.jsssisss', 1, '2022-01-08 23:19:29', '2022-01-08 23:19:29'),
(11, 'prueba', 'luis.hernandsssez.jsssisss', 1, '2022-01-08 23:19:38', '2022-01-08 23:19:38'),
(12, 'prueba', 'luiss.hernandsssez.jsssisss', 1, '2022-01-08 18:19:58', '2022-01-08 18:19:58'),
(13, 'prueba', 'd', 1, '2022-01-08 18:20:20', '2022-01-08 18:20:20'),
(14, 'prueba', 'dddds', 1, '2022-01-08 18:21:34', '2022-01-08 18:21:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `virtual_services`
--

CREATE TABLE `virtual_services` (
  `id` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `name_servicio` varchar(100) NOT NULL,
  `service_user` varchar(100) NOT NULL,
  `password` longtext NOT NULL,
  `service_strategy` varchar(253) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user_create` int(11) NOT NULL,
  `id_user_edit` int(11) NOT NULL,
  `delected` int(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `virtual_services`
--

INSERT INTO `virtual_services` (`id`, `subject`, `name_servicio`, `service_user`, `password`, `service_strategy`, `id_client`, `id_user_create`, `id_user_edit`, `delected`, `updated_at`, `created_at`) VALUES
(1, 'EbChatBaseAutenticacion', 'Prueba Konecta', 'pruebaewewewe', 'prueba123', 'dwedwe', 1, 1, 1, 0, '2021-12-07 00:00:00', '2021-12-07 00:00:00'),
(11, 'EbChatPre', 'api', 'dewdwdddddde', 'eyJpdiI6IkxxeXczeWxFM21IMHZsSVpLbGdlbmc9PSIsInZhbHVlIjoiMFFHSmhtTFc0UExRanJaYVVpemc4Zz09IiwibWFjIjoiNThjODcxMDQ2YjliMzAyY2FjNjU2YjNlNmY4M2ZiODgwMjU1MjdkNzQ0NzdjYjk4ZjQ0Y2Y0ODE1MjFiZDEzMyIsInRhZyI6IiJ9', 'utl_prueba', 1, 1, 1, 0, '2021-12-22 00:00:00', '2021-12-22 00:00:00'),
(12, 'ChatPrueba', 'Prueba Konecta', 'prueba', 'eyJpdiI6ImNDQVdLMlR0R3MyYUN2Z0VDd0JlN0E9PSIsInZhbHVlIjoiQnIvMlVaaDRBQ0VTU08yZnBhOXk0dz09IiwibWFjIjoiNjFhNGRhYjFjZTg4NzI4ZGNjNjFiYTNjNzU4MDY3ZmI5YzUwYmM3MjA0ZjU3NTQ1MDNlMTRmYTExMDEyZDJjOCIsInRhZyI6IiJ9', 'https://widget.grupokonecta.co/genesys/2/chat/allus', 1, 1, 1, 0, '2021-12-22 00:00:00', '2021-12-22 00:00:00'),
(13, 'EbChatBaseAutenticacion', 'Prueba Konecta', 'pruebjkjka', 'eyJpdiI6Ik1OZFlMcjZ0U09PQ3BNVzhaSEV0MUE9PSIsInZhbHVlIjoiMjQ3dy9DYVYyVVg2NHZwR2syUkhrZz09IiwibWFjIjoiNTdlNTc0MTRlZDAxZWZmZWU4ZGJjN2IwYWNhMmJiNGM2ZTg4MGY4NGNlN2UyODdjMzAwNjZiODc2NmUxZjg3YyIsInRhZyI6IiJ9', 'https://widget.grupokonecta.co/genesys/2/chat/allus', 1, 1, 1, 0, '2021-12-26 00:00:00', '2021-12-26 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_service` (`id_service`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_user_2` (`id_user`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user_create` (`id_user_create`),
  ADD KEY `id_user_edit` (`id_user_edit`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `token_sv`
--
ALTER TABLE `token_sv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `service_id_2` (`service_id`);

--
-- Indices de la tabla `token_users`
--
ALTER TABLE `token_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `virtual_services`
--
ALTER TABLE `virtual_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_user_create` (`id_user_create`),
  ADD KEY `id_user_edit` (`id_user_edit`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del rol', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sesion`
--
ALTER TABLE `sesion`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `token_sv`
--
ALTER TABLE `token_sv`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT de la tabla `token_users`
--
ALTER TABLE `token_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del usuario', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `virtual_services`
--
ALTER TABLE `virtual_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  ADD CONSTRAINT `activitylogs_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `activitylogs_ibfk_2` FOREIGN KEY (`id_service`) REFERENCES `virtual_services` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`id_user_create`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`id_user_edit`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `token_sv`
--
ALTER TABLE `token_sv`
  ADD CONSTRAINT `token_sv_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `virtual_services` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `virtual_services`
--
ALTER TABLE `virtual_services`
  ADD CONSTRAINT `virtual_services_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clientes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `virtual_services_ibfk_4` FOREIGN KEY (`id_user_create`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `virtual_services_ibfk_5` FOREIGN KEY (`id_user_edit`) REFERENCES `users` (`id`);

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `delect_sesion` ON SCHEDULE EVERY 1 MINUTE STARTS '2022-01-07 13:35:26' ENDS '2026-07-31 23:59:59' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE token_sv SET token_sv.delected = 1 WHERE UNIX_TIMESTAMP(token_sv.date_init) < (UNIX_TIMESTAMP()-0.5*3600)$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
