-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-12-2021 a las 13:26:31
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba_bd_aut`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activitylogs`
--

CREATE TABLE `activitylogs` (
  `id_log` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `ip_usuario` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_client` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_user_create` int(11) NOT NULL,
  `id_user_edit` int(11) NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_client`, `name`, `id_user_create`, `id_user_edit`, `delected`) VALUES
(1, 'Prueba Konecta', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL COMMENT 'Id del rol',
  `rol_name` varchar(100) DEFAULT NULL COMMENT 'name del rol'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol_name`) VALUES
(1, 'Administrador'),
(2, 'Auditor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL COMMENT 'Id del usuario',
  `name` varchar(255) NOT NULL COMMENT 'name del usuario',
  `user_network` varchar(100) NOT NULL COMMENT 'Usuario de red',
  `id_rol` int(11) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `name`, `user_network`, `id_rol`, `updated_at`, `created_at`) VALUES
(1, 'Luis Hernandez', 'luis.hernandez.ji', 1, '0000-00-00', '0000-00-00'),
(4, 'dewdwe', 'luis.hernandez.ji', 1, '2021-12-03', '2021-12-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vitualservices`
--

CREATE TABLE `vitualservices` (
  `id_service` int(11) NOT NULL,
  `name_servicio` varchar(100) NOT NULL,
  `service_user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `service_strategy` varchar(255) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user_create` int(11) NOT NULL,
  `id_user_edit` int(11) NOT NULL,
  `delected` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vitualservices`
--

INSERT INTO `vitualservices` (`id_service`, `name_servicio`, `service_user`, `password`, `service_strategy`, `id_client`, `id_user_create`, `id_user_edit`, `delected`) VALUES
(1, 'Prueba Konecta', 'prueba', 'prueba123', 'url', 1, 1, 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `id_service` (`id_service`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_user_2` (`id_user`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `id_user_create` (`id_user_create`),
  ADD KEY `id_user_edit` (`id_user_edit`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `vitualservices`
--
ALTER TABLE `vitualservices`
  ADD PRIMARY KEY (`id_service`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_user_create` (`id_user_create`),
  ADD KEY `id_user_edit` (`id_user_edit`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del rol', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del usuario', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `vitualservices`
--
ALTER TABLE `vitualservices`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activitylogs`
--
ALTER TABLE `activitylogs`
  ADD CONSTRAINT `activitylogs_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE,
  ADD CONSTRAINT `activitylogs_ibfk_2` FOREIGN KEY (`id_service`) REFERENCES `vitualservices` (`id_service`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`id_user_create`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`id_user_edit`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);

--
-- Filtros para la tabla `vitualservices`
--
ALTER TABLE `vitualservices`
  ADD CONSTRAINT `vitualservices_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clientes` (`id_client`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vitualservices_ibfk_2` FOREIGN KEY (`id_user_create`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vitualservices_ibfk_3` FOREIGN KEY (`id_user_edit`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
