<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\TokenUser\JwtAuthHelperUser;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtTokenUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $response = '';
        try {

            $user = JWTAuth::parseToken()->authenticate();
            $infoToken = [
                'user'    => $user->id,
                'user_ip' => $request->ip(),
                'token'   => $request->bearerToken()
            ];

            $objJwtAuthHelper = new JwtAuthHelperUser();
            $boolValidToken = $objJwtAuthHelper->checkToken($infoToken);
            if($boolValidToken){
                $response = $next($request);
            } else {
                $response = response()->json(['status' => '999', 
                'message' => 'Sesion invalida. '
                ], 200);
            } 

        } catch (\Throwable $th) {
            $response = response()
                ->json(['status' => '999', 
                'message' => 'Sesion invalida. '
            ], 200);
        }

        return $response;
    }
}
