<?php

namespace App\Models\SV;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Cliente extends Model
{
    protected $table = 'clientes';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'id_user_create',
        'id_user_edit',
        'delected',
    ];

    public function user_create() {
        return $this->belongsTo(User::class, "id_user");
    }
    
    public function user_edit() {
        return $this->belongsTo(User::class, "id_user_edit");
    }

}
