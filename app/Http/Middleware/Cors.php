<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->headers->set('Access-Control-Allow-Origin',env('APP_URL'));
        $response->headers->set('Access-Control-Allow-Methods','GET, PUT, POST, DELETE, HEAD, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With, Application');
        $response->headers->set('Cache-Control', 'must-revalidate, no-cache, no-store, private');
        $response->headers->set('Content-Security-Policy', "default-src 'none'; frame-src 'self'; script-src 'self' 'unsafe-eval' 'unsafe-inline' https://maps.googleapis.com/; style-src 'self' 'unsafe-inline' https://maxcdn.bootstrapcdn.com/ https://fonts.googleapis.com/ https://use.fontawesome.com/ https://netdna.bootstrapcdn.com/; media-src 'self'; font-src 'self' https://maxcdn.bootstrapcdn.com/ https://fonts.googleapis.com/ https://fonts.gstatic.com/ https://use.fontawesome.com/; img-src 'self' data:; frame-ancestors 'self'; form-action 'self'; connect-src 'self' ".env('APP_URL'));
        $response->headers->set('Strict-Transport-Security', "max-age=31536000; includeSubDomains; preload;");
        $response->headers->set('Pragma', "no-cache");
        $response->headers->set('X-Frame-Options', "SAMEORIGIN");
        $response->headers->set('X-Content-Type-Options', "nosniff");
        $response->headers->set('X-Xss-Protection', "1; mode=block");
        $response->headers->set('Permissions-Policy', "fullscreen=none ,accelerometer=none, battery=none, camera=none, geolocation=none, gyroscope=none, magnetometer=none, microphone=none");

        return $response;

    }
}
