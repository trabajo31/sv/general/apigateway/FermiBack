<?php

namespace App\Http\Controllers\ClientController;

use App\BL\Client\ClientBL;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Generic\ResponseController;
use App\Http\Requests\Clients\ClientsRequest;
use App\Http\Requests\SV\VirtualServiceRequest;

/** 
 * Controlador para las gestiones de los servicios virtuales
 * 
 * 
 * @package App\Http\Controllers\SV
 * @author Luis Manuel Hernandez Jimenez (luis.hernandez@grupokonecta.com)
 * @date 01/01/2016
 * */
class ClientController extends Controller
{

    /**
     * Controlador para añadir un cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param VirtualServiceRequest $request
     * @return json $response
     */
    public function addClient(ClientsRequest $request) {
        $objRequest = $request->all();
        $objTosave = [
            'name' => $objRequest['name']
        ];
        $response = ClientBL::add($objTosave);
        return  ResponseController::objectResponse($response);
    }

    /**
     * Controlador para actualizar la info de un cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param VirtualServiceRequest $request
     * @return json $response
     */
    public function update(ClientsRequest $request) {
        $data = $request->all();
        $response = ClientBL::update($data);
        return ResponseController::objectResponse($response, 200);
    }

    /**
     * Controlador para eliminar la info de un cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param VirtualServiceRequest $request
     * @return json $response
     */
    public function delete() {
        $objRequest = request()->get('id');
        $response = ClientBL::delete($objRequest);
        return ResponseController::objectResponse($response);
    }

    /**
     * Controlador para actualizar la info de un cliente
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param VirtualServiceRequest $request
     * @return json $response
     */
    public function reactive() {
        $objRequest = request()->get('id');
        $response = ClientBL::reactive($objRequest);
        return ResponseController::objectResponse($response);
    }
    
    /**
     * Controlador para obtener el listado de clientes
     *
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    public function getAll() {
        $response = ClientBL::getAll();
        return ResponseController::objectResponse($response);
    }

}
