<?php

namespace App\Helpers;

class EncryptDecryptHelper
{

    public static function encrypt($value)
    {
        $salt = openssl_random_pseudo_bytes(256);
        $iv = openssl_random_pseudo_bytes(16);

        $iterations = 999;  
        $key = hash_pbkdf2("sha512", env('CRYPTO_KEY'), $salt, $iterations, 64);

        $encrypted_data = openssl_encrypt($value, 'aes-256-cbc', hex2bin($key), OPENSSL_RAW_DATA, $iv);

        $data = array("ciphertext" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "salt" => bin2hex($salt));
        return base64_encode(json_encode($data));
    }

    /**
     * Decrypt a previously encrypted value
     * @param string $jsonStr Json stringified value
     * @return mixed
     */
    public static function decrypt($jsonStr)
    {
        $jsondata = json_decode(base64_decode($jsonStr), true);
        try {
            $salt = hex2bin($jsondata["salt"]);
            $iv  = hex2bin($jsondata["iv"]);          
        } catch(\Exception $e) { return null; }

        $ciphertext = base64_decode($jsondata["ciphertext"]);
        $iterations = 999; 

        $key = hash_pbkdf2("sha512", env('CRYPTO_KEY'), $salt, $iterations, 64);

        return openssl_decrypt($ciphertext , 'aes-256-cbc', hex2bin($key), OPENSSL_RAW_DATA, $iv);
    }
}