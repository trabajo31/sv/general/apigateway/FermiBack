<?php

namespace App\Helpers\CurrentUser;
use Tymon\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;

class CurrentUser
{
    public static function getCurrentUser() {
        return FacadesJWTAuth::parseToken()->authenticate();
    }
}
