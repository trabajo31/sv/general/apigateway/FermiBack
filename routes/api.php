<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SV\VirtualServiceController;
use App\Http\Controllers\AuthController\User\AuthUserController;
use App\Http\Controllers\ClientController\ClientController;
use App\Http\Controllers\User\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    /**
     * Rutas para la autenticacion de un servicio virtual
     * Routes to authenticate a services virtual
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @param user $
     * @param password $
     * @return Boolean $
     */
    Route::post('auth/login', [AuthUserController::class, 'login']);
    Route::post('auth/logout', [AuthUserController::class, 'logout']);


    Route::group(['middleware' => 'JwtAuthUsers', 'prefix' => 'front' ], function () {

        Route::post('auth/me', [AuthUserController::class, 'me']);
        
            Route::group(['prefix' => 'users'], function () {

                Route::post('register', [UserController::class, 'addUser']);
                
                Route::post('list', [UserController::class, 'getAll']);
                
                Route::post('update', [UserController::class, 'update']);

                Route::post('delete', [UserController::class, 'delete']);

            });
           

            Route::group(['prefix' => 'service'], function () {
                
                Route::post('register', [VirtualServiceController::class, 'addService']);
            
                Route::post('list', [VirtualServiceController::class, 'getAll']);

                Route::post('listUserSV', [VirtualServiceController::class, 'getAllusersSV']);
               
                Route::post('search', [VirtualServiceController::class, 'getServiceById']);
                
                Route::post('update', [VirtualServiceController::class, 'update']);
                
                Route::post('reactive', [VirtualServiceController::class, 'reactive']);//actualizar
                
                Route::post('delete', [VirtualServiceController::class, 'delete']);//actualizar
            
            });


            Route::group(['prefix' => 'clients'], function () {
                
                Route::post('list', [ClientController::class, 'getAll']);
                
                Route::post('update', [ClientController::class, 'update']);
                
                Route::post('add', [ClientController::class, 'addClient']);

                Route::post('delete', [ClientController::class, 'delete']);

                Route::post('reactive', [ClientController::class, 'reactive']);

            });
               

    });





