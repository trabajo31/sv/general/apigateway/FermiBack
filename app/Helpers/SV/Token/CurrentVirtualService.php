<?php

namespace App\Helpers\SV\Token;

use Tymon\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;

class CurrentVirtualService
{
    public static function getCurrentUser() {
        return FacadesJWTAuth::parseToken()->authenticate();
    }
}
