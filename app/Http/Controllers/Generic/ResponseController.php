<?php

namespace App\Http\Controllers\Generic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResponseController extends Controller
{
    /**
     * Metodo para centralizar las respuestas de la api
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @param array $data Datos del cliente a instertad en BD
     * @return Response App\Http\Controllers\Generic\ResponseController
     */
    public static function objectResponse($res) {
        $response = [];
        if ($res['status'] === 500) {
            $data   = null;
        } else if($res['status'] === 400) {
            $data   = null;
        } else if($res['status'] === 202){
            $data   = $res['data'];
        } else if($res['status'] === 200){
            $data   = $res['data'];
        } else {
            $data = null;
        }
        $response['data'] = $data;
        $response['message'] = $res['msn'];
        $response['status'] = $res['status'];
        
        return response()->json($response);
    }
}
