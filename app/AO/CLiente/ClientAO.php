<?php

namespace App\AO\CLiente;

use App\Models\SV\Cliente;

class ClientAO
{

    /**
     * Consulta para obtener un cliente de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @param string $clientName con el nombre del cliente a buscar
     * @return Modelo App\Models\SV\Cliente
     */
    public static function getClientByName($clientName) {
        return Cliente::where('name', $clientName)->get()->first();
    }


    /**
     * Consulta para obtener un cliente de la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @param array $objRequest con los datos del cliente a instertad en BD
     * @return Modelo App\Models\SV\Cliente
     */
    public static function add($objRequest) {
        return Cliente::insert($objRequest);
    }

    /**
     * Consulta para obtener todos los clientes la base de datos
     * 
     * @date 30/11/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @return Modelo App\Models\SV\Cliente 
     */
    public static function getAll() {
        return Cliente::where('delected', 0)->with(['user_create', 'user_edit'])->get();
    }

    /**
     * Consulta para obtener un cliente de la base de datos
     * pasando como parametro el id
     * 
     * @date 30/12/2021
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @param int $id con el ID del cliente a buscar en BD
     * @return Modelo App\Models\SV\Cliente
     */
    public static function getClientById($id) {
        return Cliente::where('id', $id)->where('delected',0)->first();
    }

    /**
     * Consulta para obtener un cliente eliminado de la base de datos
     * pasando como parametro el id
     * 
     * @date 04/11/2022
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para obtener un cliente de la base de datos
     * @param int $id con el ID del cliente a buscar en BD
     * @return Modelo App\Models\SV\Cliente
     */
    public static function getClientDeleteById($id) {
        return Cliente::where('id', $id)->where('delected',1)->first();
    }

    /**
     * Consulta para actualizar los datos de un cliente de la base de datos
     * 
     * @date 04/01/2022
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para actualizar los datos de un cliente de la base de datos
     * @param Modelo $client Cliente a actualizar a buscar en BD
     * @param array $updated con los datos actualizar
     * @return boolean
     */
    public static function update($client, $update) {
        return $client->update($update);
    }

    /**
     * Consulta para realizar la eliminación de manera logica 
     * de un cliente de la base de datos
     * 
     * @date 04/01/2022
     * @author Luis Manuel Hernandez Jimenez
     * @description Consulta para realizar la eliminación de manera logica 
     * de un cliente de la base de datos
     * @param $id $client ID del cliente a eliminar
     * @return boolean
     */
    public static function delete($client) {
        return $client->update(['delected' => 1]);
    }

    /**
     * Consulta para realizar la reactivación de manera logica 
     * de un cliente de la base de datos
     * 
     * @date 04/01/2022
     * @author Luis Manuel Hernandez Jimenez
     * @description  Consulta para realizar la reactivación de manera logica 
     * de un cliente de la base de datos
     * @param $id $client ID del cliente a eliminar
     * @return boolean
     */ 
    public static function reactive($client) {
        return $client->update(['delected' => 0]);
    }

}
