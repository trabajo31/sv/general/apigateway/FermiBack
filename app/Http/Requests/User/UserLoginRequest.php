<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'user_network' => 'required|max:100|regex:/^[\pL\s.0-9]*$/u',
            'password' => 'required|max:50',
        ];
    }

    public function messages()
    {
        
        return [
            'user_network' => 'El Usuario de red es obligatorio.',
            'user_network.max'      => 'El Usuario de red  es de maximo 45 caracteres',
            'user_network.regex'      => 'El campo Usuario de red debe ser texto',
            'password.required' => 'El Nombre es obligatoria.',
            'password.max'      => 'El campo Nombre de red es de maximo 50 caracteres',
            'password.regex'      => 'El campo Nombre de red debe ser solo letras',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status' => 500,
            'message' => $validator->errors()->all()
        ], 200));
    }
}
