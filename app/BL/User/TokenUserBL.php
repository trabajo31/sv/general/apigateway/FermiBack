<?php

namespace App\BL\User;

use App\AO\User\TokenUsers\TokenUsersAO;
use Carbon\Carbon;

use App\Http\Controllers\Generic\ResponseController;
use Illuminate\Support\Facades\Log;

class TokenUserBL {

    private static $response = [];
    private static $excepcion = ['msm' => 'Error al consultar en la Base de Datos', 'status' => 500];


    public static function setToken($infoToken) {
       try {
            $objToken = [
                'user_ip'  => $infoToken['user_ip'],
                'token'    => $infoToken['token'], 
                'user_id'  => $infoToken['user'],
                'date'     => Carbon::now('UTC')->subHours(5)->toDateTimeString(),
                'delected' => 0
            ];
            TokenUsersAO::setToken($objToken);
       } catch (\Throwable $e) {
        self::$response = self::$excepcion;
        Log::error($e . 'function setToken() in app\BL\User\TokenUserBL.php');
       }
    }

    public static function validateConversation($infoToken) {
        try {
            self::$response = TokenUsersAO::countLoggedRegister($infoToken);
        } catch (\Throwable $e) {
            self::$response = self::$excepcion;
            Log::error($e . 'function validateConversation() in app\BL\User\TokenUserBL.php');
        }
        return self::$response;
    }

    public static function disableAllTokens($infoToken) {
        try {
            $objToken = [
                'state' => 0,
                'updated_at' => Carbon::now('UTC')->subHours(5)->toDateTimeString()
            ];
            self::$response = TokenUsersAO::disableAllTokens($infoToken,$objToken);
        } catch (\Throwable $e) {
            self::$response = self::$excepcion;
            Log::error($e . 'function validateConversation() in app\BL\User\TokenUserBL.php');
        }
        return self::$response;
    }

}
