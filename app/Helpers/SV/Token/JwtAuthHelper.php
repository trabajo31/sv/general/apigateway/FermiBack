<?php

namespace App\Helpers\SV\Token;

use App\BL\SV\Token\TokenServiceBL;

class JwtAuthHelper
{
    public function saveToken($infoToken){
        TokenServiceBL::setToken($infoToken);
    }
    
    public function checkToken($infoToken){
        return TokenServiceBL::validateConversation($infoToken);
    }

    
}